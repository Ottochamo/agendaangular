import { Routes  } from '@angular/router';

import { TareaComponent } from './tarea.component';
import { TareaEditarComponent } from './editar/tarea-editar.component';
import { TareaNuevoComponent } from './nuevo/tarea-nuevo.component';


export const TAREA_ROUTES: Routes = [
    {path: '', component: TareaComponent},
    {path: 'nuevo', component: TareaNuevoComponent},
    {path: 'editar/:id', component: TareaEditarComponent }

];