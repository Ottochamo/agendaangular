import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Usuario } from '../../models/usuario.model';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public model = new Usuario();

  constructor(private route: ActivatedRoute,
    private router: Router, 
    private usuarioService: UsuarioService) { }

  ngOnInit() {}

  public crearCuenta() {
    this.usuarioService.postUsuarios(this.model)
      .subscribe(success => {
        console.log(success);
        this.router.navigate(['Login']);
      }, error => {
        console.log(error)
      }, () => {
        console.log('finish')
      })
  }

}
