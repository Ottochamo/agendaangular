import {Injectable} from '@angular/core';

import {Http, Response, 
        Headers, RequestOptions } from '@angular/http';

import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoriaService {
    private uriCategoria: string = 'http://localhost:3000/api/v1/categoria/';
    
    constructor(private http: Http) {}
    
    public getCategorias() {
        let options = this.returnOptionsConfig();
        let id = localStorage.getItem('user');
        let uri = 'http://localhost:3000/api/v1/categorias/' + id;

        return this.http.get(uri, options)
            .map(this.extractData).catch(this.handleError);
    }

    public getCategoria(id: string) {
        let options = this.returnOptionsConfig();
        let uri = this.uriCategoria + id;

        return this.http.get(uri, options)
            .map(this.extractData).catch(this.handleError);

    }

    public postCategoria(data: any) {
        let options = this.returnOptionsConfig();
        let user = localStorage.getItem('user');
        data.idUsuario = user;

        return this.http.post(this.uriCategoria, data, options)
            .map(this.extractData).catch(this.handleError);

    }

    public putCategoria(data: any) {
        let options = this.returnOptionsConfig();

        return this.http.put(this.uriCategoria + data.idCategoria, data, options)
            .map(this.extractData).catch(this.extractData);

    }

    public deleteCategoria(id: any) {
        let options = this.returnOptionsConfig();
        return this.http.delete(this.uriCategoria + id, options)
            .map(this.extractData).catch(this.handleError);

    }

    private returnOptionsConfig(): RequestOptions {
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        headers.append('authorization', token);
        let options = new RequestOptions({'headers': headers});

        return options;

    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
  }

}