import { Routes  } from '@angular/router';

import { CategoriaComponent } from './categoria.component';
import {CategoriaEditarComponent} from './children/edit/categoria-editar.component';
import {CategoriaNuevoComponent} from './children/new/categoria-new.component';
import {CategoriaEliminarComponent} from './children/delete/categoria-delete.component';

export const CATEGORIA_ROUTES: Routes = [
    {path: '', component: CategoriaComponent},
    {path: 'nuevo', component: CategoriaNuevoComponent},
    {path: 'editar/:id', component: CategoriaEditarComponent },
    {path: 'eliminar/:id', component: CategoriaEliminarComponent}

];