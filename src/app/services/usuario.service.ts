import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class UsuarioService {
  uriUsuario = 'http://localhost:3000/api/v1/usuario/';
  uri = 'http://localhost:3000/';
  usuarios: any[];

  constructor(
    private http: Http,
    private router: Router
  ) {}

  public autenticar(usuario: any) {
    let uri2 = this.uri + 'auth/';
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({headers: headers});

    let data = JSON.stringify(usuario);

    this.http.post(uri2, data, options)
    .subscribe( response => {
      console.log(response.json());
      if (response.json().estado) {
          this.setToken(response.json().token);
          this.setCurrentUser({ 
          //nick: response.json().nick
          idUsuario: response.json().idUsuario
      });
        this.router.navigate(['Dashboard']);
      } else {
        this.router.navigate(['/']);
      }
    }), error => {
      console.log(error.text());
    }

  }

  public setToken(token: string) {
    if (localStorage.getItem('token') != token) {
      localStorage.removeItem('token');
      localStorage.setItem('token', token);
    }
  }

  public setCurrentUser(usuario :any) {
    let json = JSON.parse(JSON.stringify(usuario));
    localStorage.setItem('user', usuario.idUsuario);
  }

  public verificarSesion(): boolean {
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

  getUsuarios() {
    return this.http.get(this.uriUsuario)
      .map(this.extractData).catch(this.handleError);
  }

  getUsuario(id: any) {
    let headers = this.returnOptionsConfig();

    let options = new RequestOptions({'headers': headers});


    return this.http.get(this.uriUsuario + id, options)
      .map(this.extractData).catch(this.handleError);
  }

  postUsuarios(data: any) {
    let headers = new Headers({'Content-type': 'application/json'});
    let options = new RequestOptions({ 'headers': headers });
    return this.http.post(this.uriUsuario, data, options).map(this.extractData)
    .catch(this.handleError);
  }

  putUsuario(data: any) {
    let options = new 
      RequestOptions({'headers': this.returnOptionsConfig()});

    return this.http.put(this.uriUsuario + data.idUsuario, data, options)
      .map(this.extractData)
        .catch(this.handleError);
    
  }

  deleteUsuario(id: number) {
    let headers = new Headers({'Content-type': 'application/json'});
    let options = new RequestOptions({'headers': headers});
    let uri = this.uriUsuario + '/' + id;
    return this.http.delete(uri, options)
      .map(this.extractData).catch(this.handleError)
  }

  uploader(file: any) {
    
    let token = localStorage.getItem('token');
    let headers = new Headers({'Accept': 'application/json'});
    headers.append('authorization', token);
    let options = new RequestOptions({'headers': headers});

    let formData = new FormData();

    formData.append('file', file, file.name);

    return this.http.post(this.uri + 'uploadUser/', formData, options)
      .map(this.extractData).catch(this.handleError);
  }

  cerrarSesion() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.router.navigate(['/']);
  }

  private returnOptionsConfig() {
    let token = localStorage.getItem('token');
    let headers = new Headers({'Content-Type': 'application/json'});
    headers.append('authorization', token);

    return headers;

  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
