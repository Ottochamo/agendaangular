import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Tarea } from '../../../../models/tarea.model';
import { TareaService } from '../../../../services/tarea.service';
import { PrioridadService } from '../../../../services/prioridad.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-editar',
  templateUrl: './tarea-editar.component.html',
  styleUrls: ['./tarea-editar.component.css']
})
export class TareaEditarComponent implements OnInit {

  public model = new Tarea();
  public prioridades: Array<any>;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tareaService: TareaService,
    private prioridadService: PrioridadService
  ) { 
    this.route.paramMap.switchMap((params: ParamMap) =>
    this.tareaService.getTarea(params.get('id')))
      .subscribe(success => {
        console.log(JSON.stringify(success));
        this.model = success;
      }, error => {
        console.log(error);
      })
      this.getPrioridades();
  }

  ngOnInit() {
    
  }

  public editarTarea() {
    this.tareaService.putTarea(this.model)
      .subscribe(success => {
        this.router.navigate(['Dashboard/tarea']);
      }, error => {
        console.log(error);
      })
  } 

  public getPrioridades() {
    this.prioridadService.getPrioridades()
      .subscribe(success => {
        this.prioridades = success
      }, error => {
        console.log(error);
      })
  }

}
