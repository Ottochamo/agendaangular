import { Routes  } from '@angular/router';

import { UsuarioDetalleComponent } from './usuario-detalle.component';
import { UsuarioEditarComponent } from './usuario-editar.component';
import { UsuariosComponent } from './usuarios.component';


export const USUARIO_ROUTES: Routes = [
    {path: ':id', component: UsuarioEditarComponent}
];