import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { Usuario } from '../../../models/usuario.model';
import { UsuarioService } from '../../../services/usuario.service';

@Component({
    selector: 'app-usuario-editar',
    templateUrl: './usuario-editar.component.html',
    providers: [UsuarioService]
})
export class UsuarioEditarComponent implements OnInit {
    public model = new Usuario();
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private usuarioService: UsuarioService
    ) { }

    ngOnInit() {
        this.route.paramMap.switchMap((params: ParamMap) => 
        this.usuarioService.getUsuario(params.get('id'))).subscribe(
            success => {
                this.model = success;
            }, error => {
                console.log(error);
            }
        );
     }

    public editarUsuario() {
        this.usuarioService.putUsuario(this.model).subscribe(success => {
            this.usuarioService.cerrarSesion();
        }, error => {
            console.log(error);
        })
    }

    public editarFoto(event) {
        console.log(event.target.files[0]);
         if (event.target.files.length !== 0) {
             this.usuarioService.uploader(event.target.files[0])
                .subscribe(success => {
                    this.model.foto = success.path;
                }, error => {
                    console.log(error);
                })
         }
    }

}