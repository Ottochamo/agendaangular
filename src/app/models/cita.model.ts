export class Cita {
    constructor(
        public idCita?: number,
        public idContacto?: number,
        public lugar?: string,
        public descripcion?: string,
        public fecha?: Date
    ) {}
}