import { RouterModule, Routes } from '@angular/router';

import {CategoriaComponent} from './categoria/categoria.component';
import {ContactoComponent} from './contacto/contacto.component';
import {UsuariosComponent} from './usuarios/usuarios.component';


import {ContactoEditarComponent} from './contacto/children/edit/contacto-editar.component';

import {DashboardComponent} from './dashboard.component';

import { USUARIO_ROUTES } from './usuarios/usuario.routes';
import { CONTACTO_ROUTES } from './contacto/contacto.routes';
import { CATEGORIA_ROUTES } from './categoria/categoria.routes';
import { TAREA_ROUTES } from './tarea/tarea.routes';
import { CITA_ROUTES } from './cita/cita.routes';
import { HISTORIAL_ROUTES} from './historial/historial.routes';

const DASHBOARD_ROUTES: Routes = [
    {path:'Dashboard',
    children: [
        { path: '', component: DashboardComponent },
        { path: 'cuenta', children: USUARIO_ROUTES},
        { path: 'contacto', children: CONTACTO_ROUTES},
        { path: 'categoria', children: CATEGORIA_ROUTES},
        { path: 'tarea', children: TAREA_ROUTES},
        { path: 'cita', children: CITA_ROUTES },
        { path: 'historial', children: HISTORIAL_ROUTES}
     ]    
    }   
    
];

export const DASHBOARD_ROUTING = RouterModule.forChild(DASHBOARD_ROUTES);