import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Usuario } from '../../models/usuario.model';
import { UsuarioService } from '../../services/usuario.service';

@Component({
    selector: '<dashboard></dashboard>',
    templateUrl: './dashboard.component.html',
    providers: [UsuarioService]
})
export class DashboardComponent implements OnInit {

    public model = new Usuario();

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private usuarioService: UsuarioService
    ) { }

    ngOnInit() { 
        this.usuarioService.getUsuario(localStorage.getItem('user'))
            .subscribe(success => {
                this.model = success;
            }, error => {
                console.log(error);
            })
    }

    cuenta() {
        this.router.navigate(['cuenta', this.model.idUsuario], {relativeTo: this.route} );
    }

}