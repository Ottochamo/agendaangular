import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class CitaService {
    uriCita = 'http://localhost:3000/api/v1/cita/';

    constructor(
        private http: Http,
        private router: Router
    ) {}

    public getCitas() {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});

        return this.http.get(this.uriCita, options)
            .map(this.extractData).catch(this.handleError);

    }

    public getCita(id: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});
        
        return this.http.get(this.uriCita + id, options)
            .map(this.extractData).catch(this.handleError);
    }

    public postCita(data: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});

        return this.http.post(this.uriCita, data, options)
            .map(this.extractData).catch(this.handleError);

    }

    public putCita(data: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});
    
        return this.http.put(this.uriCita + data.idCita, data, options)
            .map(this.extractData).catch(this.handleError);

    }

    public deleteCita(id: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});

        return this.http.delete(this.uriCita + id, options)
            .map(this.extractData).catch(this.handleError);

    }

    private returnOptionsConfig() {
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        headers.append('authorization', token);

        return headers;
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    } 

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
         const body = error.json() || '';
         const err = body.error || JSON.stringify(body);
         errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}