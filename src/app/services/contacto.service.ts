import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ContactoService {
    //disculpen la falta de audio, hay mucho ruido en mi casa en este momento e.e
    uriContacto: string = 'http://localhost:3000/api/v1/contacto/';
    uriCategoria: string = 'http://localhost:3000/api/v1/categoria/';
    uriUpload = 'http://localhost:3000/uploadUser/';
    constructor(private http: Http) {}
  
    public agregarContacto(data: any) {
        let headers = new Headers({'Content-type': 'application/json'});
        headers.append('Authorization', localStorage.getItem('token'));
        let options = new RequestOptions({'headers': headers});

        let usuario = localStorage.getItem('user');
        data.idUsuario = usuario;
        return this.http.post(this.uriContacto, data, options)
            .map(this.extractData).catch(this.handleError);
    }

    public getContactos() {
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        headers.append('authorization', token);
        let options = new RequestOptions({'headers': headers});

        return this.http.get(this.uriContacto, options)
            .map(this.extractData).catch(this.handleError);

    }

    public getContacto(param: string) {
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        headers.append('authorization', token);
        let options = new RequestOptions({'headers': headers});

        let uri = this.uriContacto + param;

        return this.http.get(uri, options)
            .map(this.extractData).catch(this.handleError);
            
    }

    public putContacto(data: any) {
        let headers = this.returnHeadersConfig();

        let options = new RequestOptions({'headers': headers});

        return this.http.put(this.uriContacto + data.idContacto,
             data, options)
            .map(this.extractData).catch(this.handleError);

    }

    public upload(file: any) {
        let headers = new Headers();
        headers.append('Authorization', localStorage.getItem('token'))
        headers.append('Accept', 'application/json');   
        let options = new RequestOptions({'headers': headers});

        let formData = new FormData();

        formData.append('file', file, file.name);

        return this.http.post(this.uriUpload, formData, options)
            .map(this.extractData);      

    }

    public deleteContacto(data: any) {
        let options = new RequestOptions({headers: this.returnHeadersConfig()});
        return this.http.delete(this.uriContacto + data.id, options)
            .map(this.extractData).catch(this.handleError);
    }

    private returnHeadersConfig(): Headers {
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        headers.append('authorization', token);

        return headers;

    }

    private extractData(res: Response) {
        let body = res.json(); 
        return body || {};
    }

    private handleError(error: Response | any) {    
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}