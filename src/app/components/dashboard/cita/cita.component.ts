import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { CitaService } from '../../../services/cita.service';

@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.css'],
  providers: [CitaService]
})
export class CitaComponent implements OnInit {

  public citas: Array<any>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private citaService: CitaService
  ) { }

  ngOnInit() {
    this.getCitas();
  }

  private getCitas() {
    this.citaService.getCitas()
      .subscribe(res => {
        console.log(res);
        this.citas = res;
      }, error => {
        console.log(error);
      }) 
  }

  public eliminar(item: any) {
    console.log('llego')
    console.log(item.idCita)
    this.citaService.deleteCita(item.idCita)
      .subscribe(res => {
        this.getCitas();
      }, error => {
        console.log(error);
      })
  }

  private cargar(item: any) {
    this.router.navigate(['editar', item.idCita], {relativeTo: this.route});
  }
    
}
