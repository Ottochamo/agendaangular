import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UsuariosComponent }from './components/dashboard/usuarios/usuarios.component';
import { ContactoComponent } from './components/dashboard/contacto/contacto.component';
import { ContactoEditarComponent  } from './components/dashboard/contacto/children/edit/contacto-editar.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { CategoriaComponent } from './components/dashboard/categoria/categoria.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsuarioDetalleComponent } from './components/dashboard/usuarios/usuario-detalle.component';
import { UsuarioEditarComponent } from './components/dashboard/usuarios/usuario-editar.component';
import { ContactoNuevoComponent } from './components/dashboard/contacto/children/new/contacto-new.component';
import { CategoriaEditarComponent} from './components/dashboard/categoria/children/edit/categoria-editar.component';
import { CategoriaNuevoComponent} from './components/dashboard/categoria/children/new/categoria-new.component';
import { CategoriaEliminarComponent } from './components/dashboard/categoria/children/delete/categoria-delete.component';
import { CitaComponent } from './components/dashboard/cita/cita.component';
import { CitaEditarComponent } from './components/dashboard/cita/editar/editar.component';
import { CitaNuevoComponent } from './components/dashboard/cita/nuevo/nuevo.component';
import { TareaComponent } from './components/dashboard/tarea/tarea.component';
import { TareaEditarComponent } from './components/dashboard/tarea/editar/tarea-editar.component';
import { TareaNuevoComponent } from './components/dashboard/tarea/nuevo/tarea-nuevo.component';


//rutas
import { APP_ROUTING } from './app.routes';
import { DASHBOARD_ROUTING } from './components/dashboard/dashboard.routes';

// servicios
import { UsuarioService } from './services/usuario.service';
import { ContactoService } from './services/contacto.service';
import { AuthGuardService} from './services/authGuard.service';
import { CategoriaService } from './services/categoria.service';
import { TareaService } from './services/tarea.service';
import { PrioridadService } from './services/prioridad.service';
import { CitaService } from './services/cita.service';
import { HistorialService } from './services/historial.service';
import { HistorialComponent } from './components/dashboard/historial/historial.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsuariosComponent,
    ContactoComponent,
    LoginComponent,
    SignupComponent,
    CategoriaComponent,
    DashboardComponent,
    UsuarioDetalleComponent,
    UsuarioEditarComponent,
    ContactoEditarComponent,
    ContactoNuevoComponent,
    CategoriaEditarComponent,
    CategoriaNuevoComponent,
    CategoriaEliminarComponent,
    CitaComponent,
    TareaComponent,
    TareaEditarComponent,
    TareaNuevoComponent,
    CitaEditarComponent,
    CitaNuevoComponent,
    HistorialComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    APP_ROUTING,
    DASHBOARD_ROUTING,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UsuarioService, AuthGuardService, ContactoService, 
    CategoriaService, PrioridadService, TareaService, CitaService,
    HistorialService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
