import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../../services/usuario.service';

import { Usuario } from '../../../models/usuario.model';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  providers: [UsuarioService]
})

export class UsuariosComponent implements OnInit {
  usuarios: any[];
  model = new Usuario(0, '', '', '');
  error: string;

  constructor(private usuarioService: UsuarioService) {}

  ngOnInit() {
    this.usuarioService.getUsuarios().subscribe(
      usuario => this.usuarios = usuario
    )
  }

  agregarUsuario() {
    console.log(this.model);

    this.usuarioService.postUsuarios(this.model).subscribe(
      success => this.getAll()
    );

    this.model.nick = '';
    this.model.contrasena = '';

  }

  fileUpload($event) {
    if($event.target.files.length !== 0) {
        this.usuarioService.uploader($event.target.files[0]).subscribe(
          success => this.model.foto = success.path,
          error => this.error = <any>error,
          () => console.log(this.model.foto)
        );
    }
  }

  eliminar(idUsuario: number) {
    this.usuarioService.deleteUsuario(idUsuario).subscribe(
      success => console.log(success),
      error => this.error =<any>error,
      () => console.log('eliminarcion completa')
    );
  }

  getAll() {
    this.usuarioService.getUsuarios().subscribe(
      usuario => this.usuarios = usuario
    );
  }

}
