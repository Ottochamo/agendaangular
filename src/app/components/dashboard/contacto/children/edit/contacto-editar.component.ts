import { Component, OnInit } from '@angular/core';

import {Router, ActivatedRoute, ParamMap } from '@angular/router';

import 'rxjs/add/operator/switchMap';

import { ContactoService } from '../../../../../services/contacto.service';
import { CategoriaService } from '../../../../../services/categoria.service';

import { Contacto } from '../../../../../models/contacto.model';


@Component({
    templateUrl: './contacto-editar.component.html',
    providers: [ContactoService, CategoriaService]
})
export class ContactoEditarComponent implements OnInit {

    
    model = new Contacto();
    categorias: Array<any>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private contactoService: ContactoService,
        private categoriaService: CategoriaService) {}

    ngOnInit() { 
        this.getCategorias();
        this.route.paramMap.switchMap((params: ParamMap) => 
        this.contactoService.getContacto(params.get('id'))).subscribe(
            success => {
                this.model.idContacto = success[0].idContacto,
                this.model.idCategoria = success[0].idCategoria,
                this.model.nombre = success[0].nombre,
                this.model.apellido = success[0].apellido,
                this.model.direccion = success[0].direccion,
                this.model.telefono = success[0].telefono,
                this.model.correo = success[0].correo,
                this.model.foto = success[0].foto
            },
            error => {
                console.log(error)
            },
            () => {
                console.log('carga completa');
            }
        )
    }

    private getCategorias() {
        this.categoriaService.getCategorias().subscribe(
            success => {
                this.categorias = success;
            }, 
            error => {
                console.log(error);
            },
            () => {
                console.log('carga completa de categorias');
            }
        )
    }

    public editarContacto() {
        this.contactoService.putContacto(this.model).subscribe(success => {
            console.log(success);
        }, error => {
            console.log(error)
        }, () => {
            this.router.navigate(['../../'], {relativeTo: this.route});
        })
    }

    public editarFoto(event) {
        console.log(event.target.files[0]);
         if (event.target.files.length !== 0) {
             this.contactoService.upload(event.target.files[0])
                .subscribe(success => {
                    this.model.foto = success.path;
                }, error => {
                    console.log(error);
                })
         }
    }

}