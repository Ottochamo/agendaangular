import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CategoriaService } from '../../../services/categoria.service';

@Component({
  templateUrl: './categoria.component.html',
  providers: [CategoriaService]
})
export class CategoriaComponent implements OnInit {

  public categorias: Array<any>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.getCategorias();
  }

  public getCategorias() {
    this.categoriaService.getCategorias()
      .subscribe(success => {
        this.categorias = success;
      }, error => {
        console.log(error);
      }, () => {
        console.log('finish');
      })
  }

  public cargar(item: any) {
    this.router.navigate(['editar', item.idCategoria], {relativeTo: this.route});
    
  }

  public eliminar(item: any) {
    this.router.navigate(['eliminar', item.idCategoria], {relativeTo: this.route});
  }

}
