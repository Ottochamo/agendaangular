import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CategoriaService } from '../../../../../services/categoria.service';
import { Categoria } from '../../../../../models/categoria.model';

import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'selector',
    templateUrl: './categoria-editar.component.html',
    providers: [CategoriaService]
})
export class CategoriaEditarComponent implements OnInit {
    
    public model = new Categoria();
    
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private categoriaService: CategoriaService
    ) { }

    ngOnInit() {
        this.route.paramMap.switchMap((params: ParamMap) => 
            this.categoriaService.getCategoria(params.get('id')))
                .subscribe(success => {                    
                    this.model = success;
                }, error => {
                    console.log(error);
                }, () => {
                    console.log('carga completa');
                })                
    }

    public editarCategoria() {
        console.log('llego');
        this.categoriaService.putCategoria(this.model)
            .subscribe(success => {
                console.log(success);
            }, error => {
                console.log(error);
            }, () => {
                this.router.navigate(['../../'], {relativeTo: this.route});
            })
    }


}