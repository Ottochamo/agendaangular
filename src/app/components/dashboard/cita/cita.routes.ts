import { Routes  } from '@angular/router';

import { CitaComponent } from './cita.component';
import { CitaEditarComponent } from './editar/editar.component';
import { CitaNuevoComponent } from './nuevo/nuevo.component';


export const CITA_ROUTES: Routes = [
    {path: '', component: CitaComponent},
    {path: 'nuevo', component: CitaNuevoComponent},
    {path: 'editar/:id', component: CitaEditarComponent }

];