import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TareaService } from '../../../services/tarea.service';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css'],
  providers: [TareaService]
})
export class TareaComponent implements OnInit {

  tareas: Array<any>;

  constructor(
    private route: ActivatedRoute,
    private router: Router, 
    private tareaService: TareaService) { }

  ngOnInit() {
    this.getTareas();
  }

  public getTareas() {
    this.tareaService.getTareas()
      .subscribe(success => {
        this.tareas = success;
      }, error => {
        console.log(error);
      }) 
  }

  public cargar(item: any) {
    this.router.navigate(['editar', item.idTarea], {relativeTo: this.route});
  }

  public eliminar(item: any) {
    this.tareaService.deleteTarea(item.idTarea)
      .subscribe(success => {
        this.getTareas();
      });
  }

}
