import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';

import { AuthGuardService } from './services/authGuard.service';

const APP_ROUTES: Routes = [
  { path: 'Login', component: LoginComponent },
  { path: 'SignUp', component: SignupComponent},
  { path: 'Dashboard', component: DashboardComponent, 
    canActivate: [AuthGuardService], canActivateChild: [AuthGuardService]},
  { path: '',  pathMatch: 'full', redirectTo: 'Login'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
