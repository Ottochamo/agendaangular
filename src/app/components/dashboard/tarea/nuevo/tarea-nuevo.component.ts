import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { Tarea } from '../../../../models/tarea.model';
import { TareaService } from '../../../../services/tarea.service';
import { PrioridadService } from '../../../../services/prioridad.service';

@Component({
  selector: 'app-nuevo',
  templateUrl: './tarea-nuevo.component.html',
  styleUrls: ['./tarea-nuevo.component.css'],
  providers: [TareaService, PrioridadService]
})
export class TareaNuevoComponent implements OnInit {

  public model = new Tarea();
  public prioridades: Array<any>;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private tareaService: TareaService,
    private prioridadService: PrioridadService
  ) { }

  ngOnInit() {
    this.getPrioridades();
  }

  public agregarTarea() {
    this.tareaService.postTarea(this.model)
      .subscribe(success => {
        this.router.navigate(['Dashboard/tarea']);
      }, error => {
        console.log(error);
      })
  } 

  public getPrioridades() {
    this.prioridadService.getPrioridades()
      .subscribe(success => {
        this.prioridades = success
      }, error => {
        console.log(error);
      })
  }

}
