import { Component, OnInit } from '@angular/core';

import {FormGroup, FormControl, Validators } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';

import {ContactoService} from '../../../services/contacto.service';

import {CategoriaService} from '../../../services/categoria.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css'],
  providers: [ContactoService, CategoriaService]
})
export class ContactoComponent implements OnInit {

  formularioAgregar: FormGroup;
  formularioEditar: FormGroup;

  categorias: Array<any>;
  contactos: Array<any>;
  contactoCargado: any;

  constructor(
      private route: ActivatedRoute,
      private router: Router, 
      private contactoService: ContactoService,
      private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.formularioAgregar = new FormGroup({
      nombre: new FormControl(''),
      apellido: new FormControl(''),
      direccion: new FormControl(''),
      telefono: new FormControl(''),
      idCategoria: new FormControl(),
      correo: new FormControl()
    });

    this.getCategorias();
    this.getContactos();
  }

  public agregarContacto() {
    this.contactoService.agregarContacto(this.formularioAgregar.value).subscribe(
      success => this.getContactos(),
      error => console.log(error),
      () => console.log('finish') 
    )
    this.getContactos();
  }

  public cargar(item: any) {
    console.log(item)
    console.log(this.router.navigate(['editar', item.idContacto], 
    {relativeTo: this.route}));
  }

  private getContactos() {
    this.contactoService.getContactos().subscribe(
      success => this.contactos = success,
      error => console.log(error),
      () => console.log('finish GET')
    );
  }

  private getCategorias() {
    this.categoriaService.getCategorias().subscribe(
      results => this.categorias = results,
      error => console.log(error)
    );
  }

}
