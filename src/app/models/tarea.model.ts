export class Tarea {
    constructor(public idTarea?: number, public idPrioridad?: number,
         public nombre?: string, public descripcion?: string, public fecha?: Date
    ) {}
}