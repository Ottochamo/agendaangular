import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Cita } from '../../../../models/cita.model';
import { CitaService } from '../../../../services/cita.service';
import { ContactoService } from '../../../../services/contacto.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css'],
  providers: [CitaService]
})
export class CitaNuevoComponent implements OnInit {

  public contactos: Array<any>;
  public model = new Cita();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private citaService: CitaService,
    private contactoService: ContactoService
  ) { }

  ngOnInit() {
    this.getContactos()
  }

  public agregarCita() {
    this.citaService.postCita(this.model)
      .subscribe(res => {
        this.router.navigate(['../'], { relativeTo: this.route});
      }, error => {
        console.log(error);
      }, () => {
        console.log('finish');
      })
  }

  private getContactos() {
    this.contactoService.getContactos()
      .subscribe(res => {
        this.contactos = res;
      }, error => {
        console.log(error);
      })
  }

}
