import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {CategoriaService} from '../../../../../services/categoria.service';

import {Categoria} from '../../../../../models/categoria.model';

@Component({
    templateUrl: './categoria-new.component.html',
    providers: [CategoriaService]
})
export class CategoriaNuevoComponent implements OnInit {
    
    public model = new Categoria();
    
    constructor(private route: ActivatedRoute,
        private router: Router,
        private categoriaService: CategoriaService) { }

    ngOnInit() { }

    public agregarCategoria() {
        this.categoriaService.postCategoria(this.model)
            .subscribe(success => {
                console.log(success);
            }, error => {
                console.log(error);
            }, () => {
                this.router.navigate(['../'], {relativeTo: this.route});
            })
    }

}