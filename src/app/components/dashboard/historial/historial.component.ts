import { Component, OnInit } from '@angular/core';

import { HistorialService } from '../../../services/historial.service';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  public historiales: Array<any>;
  public panelclass = 'timeline-badge danger' || 'timeline-inverted';

  constructor(
    private historialService: HistorialService
  ) { }

  ngOnInit() {
    this.getHistorial();
  }

  private getHistorial() {
    this.historialService.getHistorial()
      .subscribe(res => {
        this.historiales = res;
      });
  }

}
