import { Routes  } from '@angular/router';

import { ContactoComponent } from './contacto.component';
import { ContactoEditarComponent } from './children/edit/contacto-editar.component';
import {ContactoNuevoComponent} from './children/new/contacto-new.component';


export const CONTACTO_ROUTES: Routes = [
    {path: '', component: ContactoComponent},
    {path: 'nuevo', component: ContactoNuevoComponent},
    {path: 'editar/:id', component: ContactoEditarComponent }

];