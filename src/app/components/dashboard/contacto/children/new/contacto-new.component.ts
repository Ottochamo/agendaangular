import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';

import {Contacto} from '../../../../../models/contacto.model';

import {ContactoService} from '../../../../../services/contacto.service';
import {CategoriaService} from '../../../../../services/categoria.service';

import  'rxjs/add/operator/switchMap';

@Component({
    templateUrl: './contacto-new.Component.html',
    providers: [ContactoService, CategoriaService]
})

export class ContactoNuevoComponent implements OnInit {
    
    public model: Contacto = new Contacto();
    public categorias: Array<any>;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private contactoService: ContactoService,
        private categoriaService: CategoriaService) {}

    ngOnInit() {
        this.getCategorias();
    }

    public agregarContacto() {
        this.contactoService.agregarContacto(this.model)
            .subscribe(success => {
                console.log(success);
            },
            error => {
                console.log(error);
            },
            () => {
                this.router.navigate(['../'], {relativeTo: this.route});
            });
    }

    public getCategorias() {
        this.categoriaService.getCategorias()
            .subscribe(success => {                
                this.categorias = success;
            }, error => {
                console.log(JSON.parse(<any>error));
            }, () => {
                console.log('carga exitosa');
            });
    }

    public fileUpload(event) {
        this.contactoService.upload(event.target.files[0])
            .subscribe(ruta => {
                console.log(JSON.stringify(ruta));
                this.model.foto = ruta.path
            }, error => {
                console.log(error);
            }, () => {
                console.log('peticion terminada');
            });
            
        //nos vamos al servicio de contacto    
    }


}
