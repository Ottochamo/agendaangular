import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class TareaService {
    uriTarea = 'http://localhost:3000/api/v1/tarea/';

    constructor(
        private http: Http,
        private router: Router
    ) {}

    public getTareas() {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});

        return this.http.get(this.uriTarea, options)
            .map(this.extractData).catch(this.handleError);

    }

    public getTarea(id: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});
        
        return this.http.get(this.uriTarea + id, options)
            .map(this.extractData).catch(this.handleError);
    }

    public postTarea(data: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});

        return this.http.post(this.uriTarea, data, options)
            .map(this.extractData).catch(this.handleError);

    }

    public putTarea(data: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});
    
        return this.http.put(this.uriTarea + data.idTarea, data, options)
            .map(this.extractData).catch(this.handleError);

    }

    public deleteTarea(id: any) {
        let options = new RequestOptions({headers: this.returnOptionsConfig()});

        return this.http.delete(this.uriTarea + id, options)
            .map(this.extractData).catch(this.handleError);

    }

    private returnOptionsConfig() {
        let token = localStorage.getItem('token');
        let headers = new Headers({'Content-Type': 'application/json'});
        headers.append('authorization', token);

        return headers;
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    } 

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
         const body = error.json() || '';
         const err = body.error || JSON.stringify(body);
         errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
        errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

}