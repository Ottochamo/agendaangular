export class Contacto {
    constructor(public idContacto?: number, public idCategoria?: number,
        public nombre?: string, public apellido?: string, 
        public direccion?: string, public correo?: string,
        public telefono?: string, public foto?: string) {}
}