import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Cita } from '../../../../models/cita.model';
import { CitaService } from '../../../../services/cita.service';
import { ContactoService } from '../../../../services/contacto.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class CitaEditarComponent implements OnInit {

  public contactos: Array<any>;
  public model = new Cita();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private citaService: CitaService,
    private contactoService: ContactoService
  ) { 
    this.route.paramMap.switchMap((params: ParamMap) =>
    this.citaService.getCita(params.get('id')))
      .subscribe(success => {
        console.log(JSON.stringify(success));
        this.model = success;
      }, error => {
        console.log(error);
      })
      //this.getPrioridades();
  }

  ngOnInit() {
    this.getContactos()
  }

  public editarCita() {
    this.citaService.putCita(this.model)
      .subscribe(res => {
        this.router.navigate(['../../'], {relativeTo: this.route})
      })
  }

  private getContactos() {
    this.contactoService.getContactos()
      .subscribe(res => {
        this.contactos = res;
      }, error => {
        console.log(error);
      })
  }

}
